import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    const { children } = this.props;
    return (
      <div className="widget">
        <h1>React Widget Test</h1>
        <p>{children}</p>
      </div>
    );
  }
}

export default App;
