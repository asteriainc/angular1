angular
	.module('react', [])
	.directive('reactDir', function () {
		return {
			template: '<div id="reactapp" class="react-part"></div>',
			scope: {},
			link: function (scope, el, attrs) {
				var TestElement = window.lib.App;

				const reactapp = document.getElementById('reactapp')
				ReactDOM.render(
					React.createElement(TestElement, null, React.createElement('a', { href: 'https://www.asteriainc.se' }, 'Click Me!'))
				, reactapp);
			}
		};
	});